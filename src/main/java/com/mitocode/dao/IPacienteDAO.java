package com.mitocode.dao;

import javax.ejb.Local;

import com.mitocode.model.Paciente;


@Local
public interface IPacienteDAO extends IDAO<Paciente> {

}
