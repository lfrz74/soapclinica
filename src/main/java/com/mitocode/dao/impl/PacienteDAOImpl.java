package com.mitocode.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.mitocode.dao.IPacienteDAO;
import com.mitocode.model.Paciente;

@Stateless
public class PacienteDAOImpl implements IPacienteDAO, Serializable {

	@PersistenceContext(unitName = "blogPU")
	private EntityManager em;

	@Override
	public Integer registrar(Paciente pac) throws Exception {
		em.persist(pac);
		return pac.getIdPaciente();
	}

	@Override
	public Integer modificar(Paciente pac) throws Exception {
		em.merge(pac);
		return pac.getIdPaciente();
	}

	@Override
	public List<Paciente> listar() throws Exception {
		List<Paciente> lista = new ArrayList<Paciente>();

		try {			
			Query query = em.createQuery("SELECT p FROM Paciente p");
			lista = (List<Paciente>) query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}

	@Override
	public Paciente listarPorId(Paciente paciente) throws Exception {
		Paciente per = new Paciente();
		List<Paciente> lista = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Paciente p where p.idPaciente =?1");
			query.setParameter(1, paciente.getIdPaciente());

			lista = (List<Paciente>) query.getResultList();

			if (lista != null && !lista.isEmpty()) {
				per = lista.get(0);
			}

		} catch (Exception e) {
			throw e;
		}
		return per;
	}

}
